package concurrency

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"path"
	"strconv"
	"strings"

	"google.golang.org/appengine"
)

func indexHandler(w http.ResponseWriter, r *http.Request) {
	fp := path.Join("public", "index.html")
	tmpl, err := template.ParseFiles(fp)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if err := tmpl.Execute(w, struct{}{}); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func convertHandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	amount, currency, err := sanitizeConvertArgs(r.FormValue("amount"), r.FormValue("currency"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	fixer, err := getCurrency(c, currency)
	if err != nil {
		log.Printf("Can't get currency: %s", err)
		http.Error(w, "Internal Error", http.StatusInternalServerError)
		return
	}
	convResult, err := fixer.convert(amount)
	if err != nil {
		log.Printf("Cant convert data: %s", err)
		http.Error(w, "Internal Error", http.StatusInternalServerError)
		return
	}

	if strings.HasPrefix(r.Header.Get("Accept"), "application/xml") {
		x, xmlErr := xml.MarshalIndent(convResult, "", "  ")
		if xmlErr != nil {
			http.Error(w, xmlErr.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/xml")
		fmt.Fprintln(w, "<?xml version=\"1.0\" encoding=\"utf-8\"?>")
		if _, err = w.Write(x); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		return
	}

	js, err := json.MarshalIndent(convResult, "", "  ")
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	if _, err := w.Write(js); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func sanitizeConvertArgs(amountStr, currency string) (float64, string, error) {
	if amountStr == "" {
		return 0, "", fmt.Errorf("Missing amount parameter")
	}
	if currency == "" {
		return 0, "", fmt.Errorf("Missing currency parameter")
	}

	// allow lower case currency names
	currency = strings.ToUpper(currency)
	if _, ok := supportedCurrencies[currency]; !ok {
		return 0, "", fmt.Errorf("Unsuported currency")
	}

	amount, err := strconv.ParseFloat(amountStr, 64)
	if err != nil {
		log.Printf("Can't parse amount: %s", err)
		return 0, "", fmt.Errorf("Invalid amount format")
	}
	return amount, currency, err
}
