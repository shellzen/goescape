package concurrency

import "testing"

func TestConvertion(t *testing.T) {
	f := mockFixer()
	c, err := f.convert(100)
	if err != nil {
		t.Fatalf("Error converting currency: %s", err)
	}

	if c.Converted["EUR"] != "400.00" {
		t.Fatalf("Error converting PLN to EUR, expected 400.00, but got: %s", c.Converted["EUR"])
	}
	if c.Converted["MYR"] != "300.00" {
		t.Fatalf("Error converting PLN to MYR, expected 300.00, but got: %s", c.Converted["MYR"])
	}
}

func mockFixer() fixerCurrency {
	m := fixerCurrency{
		Base:  "PLN",
		Rates: make(map[string]float64),
	}
	m.Rates["EUR"] = 4.0
	m.Rates["MYR"] = 3.0
	return m
}
