package concurrency

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"google.golang.org/appengine/aetest"
)

var (
	myAeInst aetest.Instance
)

func TestMain(m *testing.M) {
	var err error
	myAeInst, err = aetest.NewInstance(nil)
	if err != nil {
		log.Fatal(err)
	}
	code := m.Run()
	if err := myAeInst.Close(); err != nil {
		log.Fatal(err)
	}
	os.Exit(code)
}

func TestIndexHander(t *testing.T) {
	w := httptest.NewRecorder()
	reqIndex, err := myAeInst.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatalf("Failed to create reqest: %v", err)
	}

	indexHandler(w, reqIndex)
	if w.Code != 200 {
		t.Fatalf("Expected html code 200 but got: %d", w.Code)
	}
	if !strings.Contains(w.Body.String(), "Concurrency") {
		t.Fatal("Missing title in body")
	}
}

func TestConvertHander(t *testing.T) {
	var (
		w        = httptest.NewRecorder()
		amount   = 200.0
		currency = "PLN"
		url      = fmt.Sprintf("/convert?amount=%.0f&currency=%s", amount, currency)
	)
	reqConvert, err := myAeInst.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatalf("Failed to create reqest: %v", err)
	}

	convertHandler(w, reqConvert)
	if w.Code != 200 {
		t.Fatalf("Expected html code 200 but got: %d", w.Code)
	}
	ct := w.Header().Get("Content-Type")
	if ct != "application/json" {
		t.Fatalf("Invalid content-type: %s", ct)
	}

	var data convertedCurrency
	decoder := json.NewDecoder(w.Body)
	if err = decoder.Decode(&data); err != nil {
		t.Fatalf("Can't decode json response: %s", err)
	}
	if data.Amount != fmt.Sprintf("%.2f", amount) || data.Currency != currency {
		t.Fatalf("Invalid data in json: Amount:%s Currency:%s ", data.Amount, data.Currency)
	}
}

func TestMalformedConvertReqs(t *testing.T) {
	badUrls := []string{
		"/convert",
		"/convert?amount=100",
		"/convert?currency=PLN",
		"/convert?amount=PLN&currency=100",
		"/convert?currency=PLN&currency=",
		"/convert?amount=100&currency=",
	}

	for i := range badUrls {
		convertReqTest(t, badUrls[i], 400)
	}
}

func TestSupportedCurrencies(t *testing.T) {
	for k := range supportedCurrencies {
		url := fmt.Sprintf("/convert?amount=100&currency=%s", k)
		convertReqTest(t, url, 200)
	}
}

func convertReqTest(t *testing.T, url string, code int) {
	w := httptest.NewRecorder()
	req, err := myAeInst.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatalf("Failed to create req: %v", err)
		return
	}
	convertHandler(w, req)
	if w.Code != code {
		t.Fatalf("Expected html code %d but got: %d (req: %s)", code, w.Code, req.URL.String())
	}
}
