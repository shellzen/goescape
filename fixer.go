package concurrency

import (
	"encoding/json"
	"fmt"

	"golang.org/x/net/context"
	"google.golang.org/appengine/urlfetch"
)

const (
	fixerURLBase = "http://api.fixer.io/latest?base="
)

// fixerCurrency
// represent exchange rates
// from fixer.io
type fixerCurrency struct {
	Base  string             `json:"base"`
	Date  string             `json:"date"`
	Rates map[string]float64 `json:"rates"`
}

// convertedCurrency
// represent conversion results
type convertedCurrency struct {
	Amount    string    `json:"amount" xml:"amount"`
	Currency  string    `json:"currency" xml:"currency"`
	Converted StringMap `json:"converted" xml:"converted"`
}

func getCurrency(c context.Context, baseCurrency string) (*fixerCurrency, error) {
	var currentFixer fixerCurrency
	client := urlfetch.Client(c)
	resp, err := client.Get(fixerURLBase + baseCurrency)
	if err != nil {
		return nil, err
	}
	decoder := json.NewDecoder(resp.Body)
	if err = decoder.Decode(&currentFixer); err != nil {
		return nil, fmt.Errorf("Can't Unmarshal fixer response: %s", err)
	}
	return &currentFixer, nil
}

func (f fixerCurrency) convert(amount float64) (convertedCurrency, error) {
	c := convertedCurrency{
		Currency:  f.Base,
		Amount:    fmt.Sprintf("%.2f", amount),
		Converted: make(map[string]string),
	}
	for currency, rate := range f.Rates {
		c.Converted[currency] = fmt.Sprintf("%.2f", rate*amount)
	}
	return c, nil
}
